const minScreenSize= '320px';
const maxScreenSize= '1920px';

const screenBoundary = {minvw:minScreenSize,maxvw:maxScreenSize}

/* definition des typo fluides

    génere les classes text-[BREAKPOINT]-fluid et leading-[BREAKPOINT]-fluid
    
    ne pas oublier d'ajouter la contrepartie dans resources/css/app.css
    .typo-BREAKPOINT_NAME {
        @apply text-title-fluid leading-title-fluid ;
    }


    What we got on old project:
    sm: 
    md: 16px (1em)
    lg: 27.2px (1.7em)
    xl:

*/
const textSizes = {
    sm: { // 20px
        min: '12px', // 0.75rem
        max: '14px', // 0.875rem
        ...screenBoundary
    },

    md: { //16px
        min: '16px', //
        max: '18px', //
        ...screenBoundary
    },

    lg: { //26px
        min: '27.2px', //
        max: '37.2px', //
        ...screenBoundary
    },

    xl: { //50px
        min: '32px', //
        max: '42px', //
        ...screenBoundary
    },
    xxxl: { //50px
        min: '30px', //
        max: '120px', //
        ...screenBoundary
    },
    title:{
        min: '32px', //
        max: '42px', //
        ...screenBoundary
    },

    bigtitle:{
        min: '55px', //
        max: '144px', //
        ...screenBoundary
    },
}

const leading = {
    sm: {
        min: '1.3em',
        max: '1.3em',
        ...screenBoundary
    },

    md: {
        min: '1.3em',
        max: '1.3em',
        ...screenBoundary
    },
    lg: {
        min: '1.3em',
        max: '1.3em',
        ...screenBoundary
    },
    xl: {
        min: '1.3em',
        max: '1.25em',
        ...screenBoundary
    },

    thight:{
        min: '0.60em',
        max: '0.70em',
        ...screenBoundary
    },
}

module.exports = {
    textSizes,leading
}